# Moise extended with Accountability Support

This is an extension of the Moise organizational model and infrastructure
(adopted in JaCaMo) that explicitly encompasses accountability.

Multiagent Systems (MAS) are valuable for
conceptualizing and implementing distributed systems,
but the current design methodologies for MAS
fall short in addressing robustness
in a systematic way at design time.
We provide a definition of accountability and show its use
for designing robust JaCaMo MAS.

Some examples are available in the examples folders.

## Installation (requires Gradle)

    git clone https://stefano_tedeschi@bitbucket.org/stefano_tedeschi/moise-accountability.git
    cd moise-accountability
    gradlew build


## Running the examples (requires Gradle)

From the root folder of the example, execute the following **Gradle task** (recommended to use the Gradle Wrapper):

    gradlew run

The task automatically downloads all the required dependencies.

Another useful task for cleaning up is:

    gradlew clean


## Contacts

For any issue, please contact [stefano.tedeschi@unito.it](mailto:stefano.tedeschi@unito.it)

---

Developed by Matteo Baldoni, Cristina Baroglio, Roberto Micalizio, Stefano Tedeschi.

---

Moise is developed and maintained by Jomi F. Hubner, Jaime S. Sichman and Olivier Boissier.  
The JaCaMo Project official webpage is: [http://jacamo.sourceforge.net/](http://jacamo.sourceforge.net/)
