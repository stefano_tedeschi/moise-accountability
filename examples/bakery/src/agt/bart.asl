{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

+!heatOven
	<- println("Heating oven...").

+!bake
	<- println("Baking bread...").

+!obligation(Ag,_,done(_,notifyFlourTypeToSeller,Ag),_) : my_name(Ag)
	<- !notifyFlourTypeToSeller;
	   goalAchieved(notifyFlourTypeToSeller).

+!notifyFlourTypeToSeller : account(flourType(FT))
	<- println("Giving account to seller...");
	   giveAccount([flourType(FT)]).

+!notifyFlourTypeToSeller : not account(flourType(FT))
	<- println("Requesting account to kneader...")
	   goalAchieved(requestFlourTypeToKneader);
	   .wait({+account(flourType(_))});
	   !notifyFlourTypeToSeller.

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }
